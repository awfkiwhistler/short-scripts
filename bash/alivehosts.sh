#!/bin/bash
# Prints a list of hosts which are up in a subnet, and their PTR records
# Requires sudo, or nmap cannot do ICMP
# Nick Shaw
# www.geekynick.co.uk

printf "All below hosts are up.\n"
hosts=`sudo nmap -sn $1 | grep -Po "(\d{1,3}\.){3}\d{1,3}"`
for host in $hosts
do
  printf "%s," $host
  ptr=`host $host | grep -oP "\s[^\s]+$"`
  if [[ $ptr == *"NXDOMAIN"* ]];
  then
    printf "No PTR Found\n"
  else
    for line in $ptr
    do
      printf "%s," $line
    done
    printf "\n"
  fi
done